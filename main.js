Grid = new Meteor.Collection("grid");


if (Meteor.isClient) {
  Meteor.startup(function(){
    
    Session.set("dataLoaded", false);
    setColorpicker();
    Deps.autorun(function(){
      Meteor.subscribe("grid", function onComplete(){
        Session.set("dataLoaded", true);
      });
    })

  });//end startup
  
  var selectedColor = "#000000";

  function setColorpicker(){
      $(".basic").spectrum({
      color: "#000000",
      change: function(color) {
          console.log(color.toHexString());
         selectedColor = color.toHexString();
          //$("#basic-log").text("change called: " + color.toHexString());
      }
  });
  }
  // templates
  Template.boxes.box = function(){
    return Grid.find({},{fields:{boxCol:1,boxID:1}});
  }
  Template.boxes.dataLoaded = function(){
    return Session.get("dataLoaded");
  }

  Template.boxes.events({
    "click .box": function () {
     //console.log(this._id);
     Grid.update(this._id,{$set:{boxCol:selectedColor}});
   }
 });


  //end templates

}//end client







